package tp1.android.mbds.fr.org.tp1android.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import tp1.android.mbds.fr.org.tp1android.classes.Person;
import tp1.android.mbds.fr.org.tp1android.adapters.PersonItemAdapter;
import tp1.android.mbds.fr.org.tp1android.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void enregistrer(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void login(View view){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    protected void onPostExecute(String theResponse) {
        super.onPostExecute(aVoid);
        showProgressDialog(false);
        ListView lst = (ListView)findViewById(R.id.listView);
        List<Person> person = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(theResponse);
            for(int i = 0; i<array.length(); i++){
                JSONObject ob = array.getJSONObject(i);
                Person p = new Person();
                p.setNom(ob.getString("nom"));

                person.add(p);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PersonItemAdapter adapter = new PersonItemAdapter(MainActivity.this, person);
        lst.setAdapter(adapter);

    }
}
