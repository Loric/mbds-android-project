package tp1.android.mbds.fr.org.tp1android.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import tp1.android.mbds.fr.org.tp1android.R;


public class RegisterActivity extends AppCompatActivity  {

    ProgressDialog progressDialog;

    Button btRegister;

    EditText nom ;
    EditText prenom;
    EditText tel;
    EditText mail;
    EditText mdp;
    EditText mdpc;
    RadioButton homme;
    RadioButton femme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

       btRegister = (Button) findViewById(R.id.register);

        nom = (EditText) findViewById(R.id.editText);
        prenom = (EditText) findViewById(R.id.editText2);
        tel = (EditText) findViewById(R.id.editText3);
        mail = (EditText) findViewById(R.id.editText4);
        mdp = (EditText) findViewById(R.id.editText5);
        mdpc = (EditText) findViewById(R.id.editText6);
        homme = (RadioButton) findViewById(R.id.radioHomme);
        femme = (RadioButton) findViewById(R.id.radioFemme);

    }

    class AsynRegister extends AsyncTask<String, Void, String>{

        private URI url;

        AsynRegister() {
            try {
                url = new URI("http://92.243.14.22/person/");
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try{
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);

                // add header

                post.setHeader("Content-Type", "application/json");
                JSONObject obj = new JSONObject();
                obj.put("prenom", params[1]);
                obj.put("nom", params[0]);
                obj.put("sexe", params[2]);
                obj.put("telephone", params[3]);
                obj.put("email", params[4]);
                obj.put("password", params[5]);
                StringEntity entity = new StringEntity(obj.toString());

                post.setEntity(entity);

                HttpResponse response = client.execute(post);
                Log.w("toto","\nSending 'POST' request to URL : " + url);
                Log.w("toto", "Post parameters : " + post.getEntity());
                Log.w("toto", "Response Code : " +
                        response.getStatusLine().getStatusCode());

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));


                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                Log.w("toto", result.toString());
                return result.toString();
            } catch (Exception e){

            }
            return null;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            showProgressDialog(true);
        }

        @Override
        protected void onPostExecute(String response){
            super.onPostExecute(response);
            showProgressDialog(false);
            Toast.makeText(RegisterActivity.this, R.string.inscription_ok, Toast.LENGTH_LONG).show();

            Intent i = new Intent(RegisterActivity.this, RelativeActivity.class);
            startActivity(i);
        }

    }

    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage(this.getResources().getString(R.string.please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }

    public void singUp(View view) {
        if(view.getId()==R.id.register){
            if(validation()) {
                String sexe = "";
                if(homme.isChecked()){
                    sexe = "Masculin";
                }else{
                    sexe = "Feminin";
                }
                String[] params = new String[]{nom.getText().toString(),prenom.getText().toString(),sexe,tel.getText().toString(),mail.getText().toString(),mdp.getText().toString()};
                new AsynRegister().execute(params);
            }
        }
    }

    public boolean validation(){
        boolean result = false;
        if(!valider()){
           result = echec();
        }else{
            result = true;
        }
        return result;
    }

    public boolean valider(){

        boolean valide = true;

        String name = nom.getText().toString();
        String firstName = prenom.getText().toString();
        String num = tel.getText().toString();
        String email = mail.getText().toString();
        String pass = mdp.getText().toString();
        String confirmedPass = mdpc.getText().toString();

        if(!homme.isChecked() && !femme.isChecked()){
            femme.setError("Selectionnez homme ou femme");
            valide = false;
        }

        if(name.isEmpty() || name.length()<3 )  {
            nom.setError("Entrez au moins 3 caractères");
            valide = false;
        }else {
            nom.setError(null);
        }

        if(firstName.isEmpty() || firstName.length()<3 )  {
            prenom.setError("Entrez au moins 3 caractères");
            valide = false;
        }else {
            prenom.setError(null);
        }

        if(num.isEmpty() || num.length() != 10 || !TextUtils.isDigitsOnly(num))  {
            tel.setError("Entrez votre numéro à 10 chiffres");
            valide = false;
        }else {
            tel.setError(null);
        }

        if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches() )  {
            mail.setError("Entrez une adresse email valide");
            valide = false;
        }else {
            mail.setError(null);
        }

        if(pass.isEmpty() || pass.length()<4 || pass.length()>10 )  {
            mdp.setError("Entrez entre 4 et 10 caractères");
            valide = false;
        }else {
            mdp.setError(null);
        }

        if(!confirmedPass.toString().equals(pass.toString()) )  {
            mdpc.setError("Les mots de passe sont différents");
            valide = false;
        }else {
            mdpc.setError(null);
        }

        return valide;
    }

    public boolean echec(){
        Toast.makeText(getBaseContext(), "Enregistrement echoué", Toast.LENGTH_LONG).show();

        btRegister.setEnabled(true);
        return false;

    }
}
