package tp1.android.mbds.fr.org.tp1android.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tp1.android.mbds.fr.org.tp1android.R;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wellcome);
    }
}
