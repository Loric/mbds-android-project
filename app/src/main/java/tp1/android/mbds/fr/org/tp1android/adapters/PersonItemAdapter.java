package tp1.android.mbds.fr.org.tp1android.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import tp1.android.mbds.fr.org.tp1android.classes.Person;
import tp1.android.mbds.fr.org.tp1android.R;

/**
 * Created by Renyusan on 30/10/2015.
 */


public class PersonItemAdapter extends BaseAdapter {

    private Context context;
    public List<Person> persons;

    public PersonItemAdapter(Context context, List<Person> person) {
        this.context = context;
        this.persons = person;
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int arg0) {
        return persons.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        View v = convertView;

        PersonViewHolder viewHolder = null;
        if(v==null){
            v = View.inflate(context, R.layout.content_relative, null);
            viewHolder = new PersonViewHolder();
            viewHolder.nom_prenom= (TextView)v.findViewById(R.id.textViewName);
//            viewHolder.date_creation= (TextView)v.findViewById(R.id.txt_date_inscription);
            v.setTag(viewHolder);
        }
        else{
            viewHolder = (PersonViewHolder) v.getTag();
        }
        Person person = persons.get(position);
        viewHolder.nom_prenom.setText(person.getComment_body_value());
        //viewHolder.date_creation.setText(comment.getName());
        return v;
    }

    class PersonViewHolder{
        TextView nom_prenom;
        TextView date_creation;
    }

}
